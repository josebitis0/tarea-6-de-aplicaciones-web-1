const bd =[
    {"Id":0,"Cedula":"1305789635", "Nombre":"Jose Bautista", "Direccion": "Nuevo Tarqui", 
    "Telefono":"0952163584", "Correo":"jose2000@hotmail.com", "Curso": "4",
    "Paralelo":"A"},
    {"Id":1,"Cedula":"1358769582", "Nombre":"Carlos Mora", "Direccion": "Ciudadela Universitaria", 
    "Telefono":"0963214532", "Correo":"carlos1213@hotmail.com", "Curso": "4",
    "Paralelo":"B"},
    {"Id":2,"Cedula":"0986574236", "Nombre":"Carlos Beltran", "Direccion": "Ciudadela Electricos", 
    "Telefono":"0954263528", "Correo":"carlos2525@hotmail.com", "Curso": "5",
    "Paralelo":"A"},
    {"Id":3,"Cedula":"1963587423", "Nombre":"Miguel Castro", "Direccion": "Nueva Esperanza", 
    "Telefono":"0975126453", "Correo":"miguel2849@outlook.com", "Curso": "5",
    "Paralelo":"A"},
    {"Id":4,"Cedula":"1985647236", "Nombre":"Maycol Cedeño", "Direccion": "Los Esteros", 
    "Telefono":"0965327412", "Correo":"maycol284@hotmail.com", "Curso": "4",
    "Paralelo":"B"},
    {"Id":5,"Cedula":"1365821369", "Nombre":"Joseph Salgado", "Direccion": "Nuevo Tarqui", 
    "Telefono":"0975632145", "Correo":"joseph4258@outlook.es", "Curso": "2",
    "Paralelo":"A"},
    {"Id":6,"Cedula":"1396542328", "Nombre":"Anthony Delgado", "Direccion": "Ciudadela Electricos", 
    "Telefono":"0976325412", "Correo":"Anthony-1213@hotmail.com", "Curso": "5",
    "Paralelo":"B"},
    {"Id":7,"Cedula":"1325874968", "Nombre":"Alex Medranda", "Direccion": "Nuevo Tarqui", 
    "Telefono":"0976325413", "Correo":"Alex_23@outlook.es", "Curso": "4",
    "Paralelo":"A"},
    {"Id":8,"Cedula":"1365249758", "Nombre":"Victor Mora", "Direccion": "Nueva Esperanza", 
    "Telefono":"0976321584", "Correo":"Victor-13@hotmail.com", "Curso": "5",
    "Paralelo":"B"},
    {"Id":9,"Cedula":"1382647593", "Nombre":"Piero Salgado", "Direccion": "Nueva Esperanza", 
    "Telefono":"0965234852", "Correo":"piero-435@hotmail.com", "Curso": "5",
    "Paralelo":"A"}
]
const estudiantes = document.querySelectorAll('.cedula_estudiante');
estudiantes.forEach((estudiante)=>{
    estudiante.addEventListener('click', (Cedula)=>{
        let id=Cedula.target.getAttribute('estu-id');
        bd.forEach((estudiante)=>{
            if(id == estudiante.Id){
                const verDetalle=Cedula.target.parentElement.lastElementChild;
                verDetalle.innerHTML=`
                                    <div class="lista">

                                        <div class="nom">
                                        <h2>Cedula:</h2>
                                        <p>${estudiante.Cedula}</p>
                                        </div>
                                        <div class="nom">
                                            <h2>Nombre:</h2>
                                            <p>${estudiante.Nombre}</p>
                                        </div>
                                        <div class="nom">
                                            <h2>Direccion:</h2>
                                            <p>${estudiante.Direccion}</p>
                                        </div>
                                        <div class="nom">
                                            <h2>Telefono:</h2>
                                            <p>${estudiante.Telefono}</p>
                                        </div>
                                        <div class="nom">
                                            <h2>Correo:</h2>
                                            <p>${estudiante.Correo}</p>
                                        </div>
                                        <div class="nom">
                                            <h2>Curso:</h2>
                                            <p>${estudiante.Curso}</p>
                                        </div>
                                        <div class="nom">
                                            <h2>Paralelo:</h2>
                                            <p>${estudiante.Paralelo}</p>
                                        </div> 
                                    </div>`

            }
        })
    })
})

